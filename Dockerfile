FROM base-vanilla-segnet:latest

WORKDIR /app

# Copy requirements.txt before copying the rest of the sources to hep pip caching dependencies
COPY ./requirements.txt .

# Install requirements
RUN pip3 install -r requirements.txt

# Copy code and files for vanilla segmentation network and watchdog
COPY ./datasets/ycb/dataset_config/*.txt ./datasets/ycb/dataset_config/
COPY ./lib ./lib
COPY ./scripts/* ./
COPY ./vanilla_segmentation ./vanilla_segmentation
COPY ./image_event_handler.py ./image_event_handler.py
COPY ./image_watcher.py ./image_watcher.py
COPY ./run_segmentation.py ./run_segmentation.py
RUN chmod +x ./run.sh

CMD /bin/bash -c ./run.sh