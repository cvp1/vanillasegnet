#!/usr/bin/env bash

IMAGE_NAME="base-vanilla-segnet:latest"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )" # directory of this script

# Build docker image
docker build -t "$IMAGE_NAME" "$SCRIPT_DIR"