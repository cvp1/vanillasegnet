import multiprocessing
import os
import time
import json
import errno

from watchdog.events import RegexMatchingEventHandler

from lib.utils import timeit
from vanilla_segmentation.segmentor import Segmentor

KEY_FILE_NAMES = 'file_names'
KEY_COLOR = 'color'
KEY_DEPTH = 'depth'
KEY_META = 'meta'
KEY_CLASS_ID = "class_id"
KEY_FRAME_ID = "frame_id"


class UnknownFileTypeEndingError(Exception):
    pass


class ImageEventHandler(RegexMatchingEventHandler):
    CONFIG_REGEX = [r".*(\d{6}(-config)\.json)$"]

    def __init__(self):
        super().__init__(self.CONFIG_REGEX)
        self.segmentor = Segmentor()

    def get_frame_config(self, config_path):
        with open(config_path) as json_file:
            frame_config = json.load(json_file)
        return frame_config

    def get_ycb_file_paths(self, ycb_frame_file_names, input_dir):
        ycb_file_paths = dict()
        for filename in ycb_frame_file_names:
            file_path = os.path.join(input_dir, filename)
            if not os.path.isfile(file_path):
                raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), file_path)
            try:
                dict_key = self.get_key_from_file_path_ending(file_path)
            except UnknownFileTypeEndingError as error:
                print(error)
                continue
            ycb_file_paths[dict_key] = file_path
        return ycb_file_paths

    def get_key_from_file_path_ending(self, file_path):
        str_path = str(file_path)
        if str_path.endswith('-{}.mat'.format(KEY_META)):
            dict_key = KEY_META
        elif str_path.endswith('-{}.png'.format(KEY_DEPTH)):
            dict_key = KEY_DEPTH
        elif str_path.endswith('-{}.png'.format(KEY_COLOR)):
            dict_key = KEY_COLOR
        else:
            raise UnknownFileTypeEndingError("File {} does not belong to YCB Video dataset.".format(file_path))
        return dict_key

    def on_created(self, event):
        print("Processing {}".format(event.src_path))
        file_size = -1
        while file_size != os.path.getsize(event.src_path):
            file_size = os.path.getsize(event.src_path)
            time.sleep(1)
        self.trigger_segmentation_multiprocess(event)

    def trigger_segmentation_multiprocess(self, event):
        process = multiprocessing.Process(name='VSN_segmentation', target=self.trigger_segmentation, args=(event,))
        process.start()
        process.join()

    @timeit
    def trigger_segmentation(self, event):
        config_path = os.path.abspath(event.src_path)
        frame_config = self.get_frame_config(config_path)
        class_id = int(frame_config[KEY_CLASS_ID])
        frame_id = frame_config[KEY_FRAME_ID]
        input_dir = os.path.dirname(config_path)
        ycb_file_paths = self.get_ycb_file_paths(frame_config[KEY_FILE_NAMES], input_dir)
        color_image_path = ycb_file_paths[KEY_COLOR]
        depth_image_path = ycb_file_paths[KEY_DEPTH]
        meta_file_path = ycb_file_paths[KEY_META]
        try:
            self.segmentor.create_partial_point_cloud(color_image_path, depth_image_path, meta_file_path,
                                                      input_dir, class_id, frame_id, plot_path=None)
        except FileNotFoundError as error:
            print(error)
