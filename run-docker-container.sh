#!/usr/bin/env bash

IMAGE_NAME="vanilla-segnet:latest"
CONTAINER_MOUNT_DIR="/app/ycb_data"
MODEL_PATH="$CONTAINER_MOUNT_DIR/models/vanilla_segnet.pth"
DEFAULT_HOST_MOUNT_DIR="$HOME/Desktop/CVP/pipeline"  # change this to where your YCB video directory is or where you want to "upload" pictures for the live segmentation
HOST_MOUNT_DIR="${DEFAULT_HOST_MOUNT_DIR}"
ENVIRONMENT="segmentation" # set this to "train", "test" or "segmentation" using the -e option
USE_GPU="" # Has to be value "True" or other (anything other than True is considered False)

while getopts ":h:e:g:" opt; do
  case $opt in
    h) HOST_MOUNT_DIR="$OPTARG"
    ;;
    e) ENVIRONMENT="$OPTARG"
    ;;
    g) USE_GPU="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done

if [ "$USE_GPU" == "True" ]; then
    echo "- GPU: True."
    GPU_ARGS="-e USE_GPU=True --gpus all"
else
    echo "- GPU: False."
    GPU_ARGS="-e USE_GPU=''"
fi
echo "- HOST_MOUNT_DIR: $HOST_MOUNT_DIR"
echo "- INPUT_DIR: $CONTAINER_MOUNT_DIR"
echo "- ENVIRONMENT: $ENVIRONMENT"
echo "- MODEL_PATH: $MODEL_PATH"

# Run docker container
docker run \
 --user "$(id -u)":"$(id -g)" \
 -e "ENVIRONMENT=$ENVIRONMENT" \
 -e "INPUT_DIR=$CONTAINER_MOUNT_DIR" \
 -e "MODEL_PATH=$MODEL_PATH" \
 -e "USE_GPU=$USE_GPU" \
 --gpus all \
 -v "$HOST_MOUNT_DIR":"$CONTAINER_MOUNT_DIR" \
 -it "$IMAGE_NAME"
