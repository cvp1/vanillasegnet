import os

from image_watcher import ImageWatcher

MODEL_PATH = 'MODEL_PATH'
USE_GPU = 'USE_GPU'
INPUT_DIR = 'INPUT_DIR'
ENV_VARS = [(MODEL_PATH, None), (USE_GPU, False), (INPUT_DIR, None)]


def check_env_variables():
    for env_var, default_value in ENV_VARS:
        if os.getenv(env_var, default_value) is None:
            raise ValueError('Environment variable "{}" is unset'.format(env_var))
    

def get_input_dir():
    input_dir = os.path.abspath(os.getenv(INPUT_DIR))
    print('Segmentation input_dir: {}'.format(input_dir))
    return input_dir


def main():
    check_env_variables()
    input_dir = get_input_dir()
    ImageWatcher(input_dir).run()


if __name__ == "__main__":
    main()
