#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )" # directory of this script
ENVIRONMENT_TRAIN="train"
ENVIRONMENT_TEST="test"
ENVIRONMENT_SEGMENTATION="segmentation"

# Check what the environment variable $ENVIRONMENT is set to and execute script accordingly
if [ "$ENVIRONMENT" == "$ENVIRONMENT_TRAIN" ]; then
    echo "Starting VanillaSegNet training..."
    bash "$SCRIPT_DIR/run_training.sh"
elif  [[ "$ENVIRONMENT" == "$ENVIRONMENT_TEST" ]]; then
    echo "Starting VanillaSegNet test..."
    bash "$SCRIPT_DIR/run_test.sh"
elif  [[ "$ENVIRONMENT" == "$ENVIRONMENT_SEGMENTATION" ]]; then
    echo "Starting VanillaSegNet segmentation run..."
    bash "$SCRIPT_DIR/run_segmentation.sh"
else
    echo "Unknown environment string passed. Exiting."
    exit 1
fi