#!/usr/bin/env bash

# Info: you can change all parameters here according to your needs
# Mind: all the paths HAVE to be under the $INPUT_DIR path specified in run-docker-container.sh for the docker
#       container to be able to access them for saving / loading data to / from your host PC.
DATASET_ROOT="$INPUT_DIR"  # path to YCB video  directory
LOGS_PATH="$INPUT_DIR/vanilla_segnet_logs"
MODEL_PATH="$MODEL_PATH"
USE_GPU="$USE_GPU"

# run test script
python3 -m vanilla_segmentation.test \
  --dataset_root "$DATASET_ROOT" \
  --logs_path "$LOGS_PATH" \
  --model_path "$MODEL_PATH" \
  --use_gpu "$USE_GPU"