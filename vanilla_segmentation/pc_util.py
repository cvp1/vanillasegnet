import os

import numpy as np
import open3d
from scipy.io import loadmat
from skimage import io


def _get_intrinsic_matrix(meta_mat):
    return meta_mat["intrinsic_matrix"]


def _get_camera_params(meta_mat):
    """
    Get camera params from ***-meta.mat file.
    :param meta_mat: ***-meta-mat file contents
    :return: camera parameters depth_factor, fx, fy, cx, cy
    """
    depth_factor = _get_depth_factor(meta_mat)
    intr_mat = _get_intrinsic_matrix(meta_mat)
    fx = intr_mat[0, 0]
    fy = intr_mat[1, 1]
    cx = intr_mat[0, 2]
    cy = intr_mat[1, 2]
    return depth_factor, fx, fy, cx, cy


def _get_depth_factor(meta_mat):
    return meta_mat["factor_depth"][0][0]


def _get_class_pose(meta_mat, class_id):
    """
    Returns the object class' pose
    :param meta_mat: *.mat file of current frame
    :param class_id: Object class id, e.g. 12 for bleach bottle
    :return: object class pose in current frame
    """
    poses = np.array(meta_mat['poses']).astype(np.float64)
    class_index = _get_class_index(meta_mat, class_id)
    class_pose = poses[:, :, class_index]
    return class_pose


def _get_class_index(meta_mat, class_id):
    """
    Returns the class index aka position of the class_id in meta_mat['cls_indexes']
    :param meta_mat: *.mat file of current frame
    :param class_id: Object class id, e.g. 12 for bleach bottle
    :return: object class index
    """
    cls_indexes = np.array(meta_mat['cls_indexes']).flatten()
    class_index = np.asarray(np.where(cls_indexes == class_id)[0])
    del cls_indexes
    if class_index.size == 0:
        return None
    return class_index[0]


def _get_class_center_2d(meta_mat, class_id):
    centers_2d = np.array(meta_mat['center']).astype(np.float64)
    class_index = _get_class_index(meta_mat, class_id)
    class_center_2d = centers_2d[:, :, class_index]
    return class_center_2d


def _get_homogeneous(mat):
    """
    Return homogeneous version of a 3x4 matrix (3 rows, 4 cols)
    :param mat: input matrix
    :return: homogeneous output matrix
    """
    assert mat.shape == (3, 4)
    return np.vstack((mat, np.array([0, 0, 0, 1])))


def _get_rot_trans_mat(meta_mat):
    """
    Returns the homogeneous rotation translation matrix from the passed *.mat file.
    :param meta_mat: *.mat file of current frame
    :return: homogeneous RT matrix
    """
    rot_trans_mat = meta_mat["rotation_translation_matrix"]
    return rot_trans_mat


def _get_homogeneous_rot_trans_mat(meta_mat):
    """
    Returns the homogeneous rotation translation matrix from the passed *.mat file
    in case the key "rotation_translation_matrix" is present. If it isn't (e.g. for data_syn)
    returns the 4x4 identity matrix.
    :param meta_mat: *.mat file of current frame
    :return: homogeneous RT matrix
    """
    if "rotation_translation_matrix" not in meta_mat:
        return np.identity(4)
    return _get_homogeneous(_get_rot_trans_mat(meta_mat))


def _get_images(video_path, frame_idx):
    """
    Returns color, depth and label images for the specified video path and frame index.
    :param video_path: Path to YCB video directory
    :param frame_idx: Index of the frame to return the images for
    :return: color_img, depth_img, label_img
    """
    color_img = open3d.io.read_image(os.path.join(video_path, '{}-color.png'.format(frame_idx)))
    depth_img = open3d.io.read_image(os.path.join(video_path, '{}-depth.png'.format(frame_idx)))
    label_img = io.imread(os.path.join(video_path, '{}-label.png'.format(frame_idx)))
    return color_img, depth_img, label_img


def _get_img_shape(img):
    return np.asarray(img, dtype=np.float64).shape


def _create_pc(color_img, depth_img, fx, fy, cx, cy, depth_factor, label_img, class_id, use_color):
    """
    Creates a point cloud from passed parameters.
    :param color_img:
    :param depth_img:
    :param fx: focal length x
    :param fy: focal length y
    :param cx: principal point x
    :param cy: principal point y
    :param depth_factor: camera depth factor
    :param label_img: predicted segmentation mask (used for masked output if class_id is given)
    :param class_id: id of YCB class of interest if masked output is wanted
    :param use_color: set to True if point cloud should be colored
    :return: (colored) open3d point cloud object
    """
    # Convert to numpy array of type float64
    im_color = np.asarray(color_img, dtype=np.float64)
    im_depth = np.asarray(depth_img, dtype=np.float64)
    im_width = im_color.shape[1]
    im_height = im_depth.shape[0]

    # Convert depth image measurements to meters by dividing by scaling factor
    Z = im_depth / depth_factor

    # Create u and v matrices
    u_range = np.arange(im_width)
    v_range = np.arange(im_height)
    u, v = np.meshgrid(u_range, v_range)

    # Calculate X and Y (matrices)
    X = ((u - cx) / fx) * Z
    Y = ((v - cy) / fy) * Z

    # Stack axes X, Y and Z to get stack with shape (480,640,3)
    stack = np.stack((X, Y, Z), axis=-1)
    
    # Apply mask if class_id is given
    if class_id != None:
        stack = _apply_class_mask(stack, label_img, class_id)

    # Create point cloud
    cloud = open3d.geometry.PointCloud()    
    stack = stack.reshape((-1, 3))

    # Assign points
    cloud.points = open3d.utility.Vector3dVector(stack)

    if use_color:
        # Assign colors if wanted
        im_color = im_color.reshape((-1, 3))
        # Convert to color range [0,1]
        im_color = im_color / 255
        cloud.colors = open3d.utility.Vector3dVector(im_color)

    return cloud


def create_point_cloud(color_img, depth_img, meta_mat, label_img=None, class_id=None, use_color=False):
    # get metadata
    depth_factor, fx, fy, cx, cy = _get_camera_params(meta_mat)
    # create point cloud
    cloud = _create_pc(color_img, depth_img, fx, fy, cx, cy, depth_factor, label_img, class_id, use_color)
    return cloud


def _apply_class_mask(array, label_img, class_id):
    label_img_mask = label_img == class_id
    mask = array[label_img_mask]
    return mask

  
def mask_pc(cloud, img_shape, label_img, class_id):

     # apply mask to PC
    point_matrix = np.asarray(cloud.points).reshape(img_shape)
    masked_points = _apply_class_mask(point_matrix, label_img, class_id)

    # masked & colored PC
    masked_cloud = open3d.geometry.PointCloud()
    masked_cloud.points = open3d.utility.Vector3dVector(masked_points)

    return masked_cloud


def rgbd_to_pc(color_img, depth_img, meta_mat):
    
    # create PC
    cloud = create_point_cloud(color_img, depth_img, meta_mat)

    # get rotation_translation_matrix and invert it
    inv_rot_trans_mat = np.linalg.inv(_get_homogeneous_rot_trans_mat(meta_mat))

    # apply inv transform to cloud
    cloud.transform(inv_rot_trans_mat)

    return cloud


def canonical_transform(cloud, meta_mat, class_id):
    cls_index = _get_class_index(meta_mat, class_id)

    # get rotation_translation_matrix
    rot_trans_mat = _get_homogeneous_rot_trans_mat(meta_mat)

    # apply transform to cloud
    cloud.transform(rot_trans_mat)

    # get groundtruth pose matrix and invert to move cloud to canonical pose
    pose_mat = meta_mat["poses"][:, :, cls_index]
    pose_mat = np.linalg.inv(_get_homogeneous(pose_mat))
    cloud.transform(pose_mat)

    return cloud


def _display_inlier_outlier(cloud, out_list):
    inlier_cloud = cloud.select_down_sample(out_list)
    outlier_cloud = cloud.select_down_sample(out_list, invert=True)

    print("Showing outliers (blue) and inliers (gray): ")
    outlier_cloud.paint_uniform_color([0, 0, 1])
    if not cloud.has_colors():
        inlier_cloud.paint_uniform_color([0.8, 0.8, 0.8])
    open3d.visualization.draw_geometries([inlier_cloud, outlier_cloud])


def _remove_outliers(masked_cloud, display=False):
    print("Statistical outlier removal x 2 -- neighbours: 1000, std: 2.0")
    cloud_first, out_list_first = masked_cloud.remove_statistical_outlier(nb_neighbors=1000, std_ratio=2.0)
    if display:
        _display_inlier_outlier(masked_cloud, out_list_first)
    cloud_final, out_list_second = cloud_first.remove_statistical_outlier(nb_neighbors=1000, std_ratio=2.0)
    if display:
        _display_inlier_outlier(cloud_first, out_list_second)
    return cloud_final


def visualize_masked_point_cloud(video_dir, frame_idx, class_id):
    # absolute path to video directory
    video_path = os.path.abspath(video_dir)

    # load image files
    color_img, depth_img, label_img = _get_images(video_path, frame_idx)
    img_shape = _get_img_shape(color_img)

    # load .mat file
    meta_mat_path = os.path.join(video_path, '{}-meta.mat'.format(frame_idx))
    meta_mat = loadmat(file_name=meta_mat_path, appendmat=False)

    # create PC
    cloud = create_point_cloud(color_img, depth_img, meta_mat)

    # get rotation_translation_matrix and invert it
    inv_rot_trans_mat = np.linalg.inv(_get_homogeneous_rot_trans_mat(meta_mat))

    # apply inv transform to cloud
    cloud.transform(inv_rot_trans_mat)

    # create camera as axes and transform
    cam_frame = open3d.geometry.TriangleMesh.create_coordinate_frame(size=0.08, origin=[0, 0, 0])
    cam_frame.transform(inv_rot_trans_mat)

    # apply mask to PC
    point_matrix = np.asarray(cloud.points).reshape(img_shape)
    masked_points = _apply_class_mask(point_matrix, label_img, class_id)

    # apply mask to color img
    color_matrix = np.asarray(color_img).reshape(img_shape)
    masked_color = _apply_class_mask(color_matrix, label_img, class_id)

    # masked & colored PC
    masked_cloud = open3d.geometry.PointCloud()
    masked_cloud.points = open3d.utility.Vector3dVector(masked_points)
    masked_cloud.paint_uniform_color([0, 1, 0])
    # masked_cloud.colors = open3d.utility.Vector3dVector(masked_color)

    cloud_final = _remove_outliers(masked_cloud, display=False)
    # cloud_final = masked_cloud
    cloud_final.paint_uniform_color([0, 0, 1])

    # bbox
    bbox = cloud_final.get_axis_aligned_bounding_box()

    # draw PC and cam
    open3d.visualization.draw_geometries([cloud, cloud_final, cam_frame])


def xyz_to_pcd(xyz_file, out_path):
    """
    Convert .xyz file to .pcd and save it
    """
    xyz = np.loadtxt(xyz_file)
    pcd = open3d.geometry.PointCloud()
    pcd.points = open3d.utility.Vector3dVector(xyz)
    open3d.io.write_point_cloud(out_path, pcd)


def read_point_cloud(file_path):
    assert os.path.isfile(file_path), "Point cloud file '{} does not exist.".format(file_path)
    pc = open3d.io.read_point_cloud(file_path)
    return np.array(pc.points)


def display_pc(np_pc):
    pcd = open3d.geometry.PointCloud()
    pcd.points = open3d.utility.Vector3dVector(np_pc)
    open3d.visualization.draw_geometries([pcd])


if __name__ == '__main__':
    video_dir = '/Users/sstoppel/Desktop/Uni/Master project CV/masterproject_data/YCB video dataset/data_syn'
    frame_idx = '000002'
    bleach_class_id = 12
    power_drill_class_id = 15
    visualize_masked_point_cloud(video_dir, frame_idx, power_drill_class_id)