import open3d as o3d
from PIL import Image
import numpy as np

PALETTE = [0, 0, 0, 128, 0, 0, 0, 128, 0, 128, 128, 0, 0, 0, 128, 128, 0, 128, 0, 128, 128,
           128, 128, 128, 64, 0, 0, 192, 0, 0, 64, 128, 0, 192, 128, 0, 64, 0, 128, 192, 0, 128,
           64, 128, 128, 192, 128, 128, 0, 64, 0, 128, 64, 0, 0, 192, 0, 128, 192, 0, 0, 64, 128]

def store_3d_vis(pcd, color_img, path):
    vis = o3d.visualization.Visualizer()
    vis.create_window()
    vis.add_geometry(pcd)
    vis.update_geometry(pcd)
    vis.poll_events()
    vis.update_renderer()
    vis.capture_screen_image(path)
    vis.destroy_window()

def _get_palette():
    palette = PALETTE
    zero_pad = 256 * 3 - len(palette)
    for i in range(zero_pad):
        palette.append(0)
    return palette

def store_colorized_mask(mask, color_img, path, class_id=None):
    palette = _get_palette()
    # mask: numpy array of the mask

    if class_id != None:
        mask = mask == class_id
        
    mask = mask.reshape(480, 640)
    mask_img = Image.fromarray(mask.astype(np.uint8)).convert('P')
    mask_img.putpalette(palette)

    mask_outline = Image.fromarray((mask==0)).convert('1')
    composed = Image.composite(color_img, mask_img.convert('RGB'), mask_outline)
    composed.save(path)
