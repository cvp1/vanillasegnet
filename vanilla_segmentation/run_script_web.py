import logging
import os
import argparse
import sys
import time

import numpy as np
import open3d as o3d
import scipy.io as scio
from PIL import Image
import torch
from torch.autograd import Variable
from torchvision import transforms

from lib.utils import timeit
from vanilla_segmentation.pc_util import create_point_cloud, canonical_transform
from vanilla_segmentation.segnet import SegNet as segnet
from vanilla_segmentation.plot_util import store_3d_vis, store_colorized_mask


def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("image_path", help="Path to color image")
    parser.add_argument("depth_path", help="Path to depth image")
    parser.add_argument("meta_path", help="Path to meta data (.mat)")
    parser.add_argument("output_path", help="Folder to save results")
    parser.add_argument("model_path", type=str, help="Path to the .pth file to load model weights from.")
    parser.add_argument("class_id", type=int, help="Class id of interest")
    parser.add_argument("frame", help="e.g  000005")
    parser.add_argument("--gpu", dest='use_gpu', action='store_true')
    parser.add_argument("--plot_path", default=None,
                        help="Folder for plots. If path is not provided, no plots will be created.")
    args = parser.parse_args()
    return args


def _get_and_preprocess_rgb(image_path):
    # load rgb image from path
    rgb_img = Image.open(image_path).convert("RGB")
    rgb = np.array(rgb_img)
    # prepare for inference in SegNet
    rgb = np.transpose(rgb, (2, 0, 1))
    norm = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
    rgb = norm(torch.from_numpy(rgb.astype(np.float32)))
    rgb = rgb.unsqueeze(0)
    return rgb, rgb_img


def _postprocess_output(seg_data):
    # adjust output from SegNet
    seg_data2 = seg_data[0]
    seg_data2 = torch.transpose(seg_data2, 0, 2)
    seg_data2 = torch.transpose(seg_data2, 0, 1)
    seg_image = torch.argmax(seg_data2, dim=-1)
    seg_image = seg_image.detach().cpu().numpy()
    return seg_image


def _get_relevant_data(image_path, depth_path, meta_path):
    # load meta, color, and depth files for point cloud generation
    meta = scio.loadmat(meta_path)
    color_img = np.array(Image.open(image_path))
    depth = np.array(Image.open(depth_path))
    return meta, color_img, depth


def _create_pc(class_id, label, color, depth, meta):
    # apply predicted mask to depth map and create point cloud for only the class
    masked_pc = create_point_cloud(color, depth, meta, label, class_id)
    # rotate output to canonical transform
    masked_pc = canonical_transform(masked_pc, meta, class_id)
    return masked_pc


@timeit
def _predict_one_pc(model, device, image_path, depth_path, meta_path, output_path, class_id, frame,
                    plot_path):
    print("...... Segmenting frame '{}'...".format(frame))

    # load and preprocess image
    rgb, rgb_img = _get_and_preprocess_rgb(image_path)

    # run inference and postprocess output
    rgb = Variable(rgb).to(device)
    seg_data = model(rgb)
    seg_image = _postprocess_output(seg_data)

    # check if wanted class is in the image
    if class_id not in seg_image:
        sys.exit(" -- The class id {} is not in frame {}. -- Aborted point cloud creation."
                 .format(class_id, frame))

    # get data needed for pc creation
    meta, color_img, depth = _get_relevant_data(image_path, depth_path, meta_path)

    # create point cloud in canonical transform for segment only
    print("...... Creating point cloud...")
    masked_pc = _create_pc(class_id, seg_image, color_img, depth, meta)

    # store segmented model
    point_cloud_output_path = os.path.join(output_path, '{}_pred_cloud.pcd'.format(frame))
    o3d.io.write_point_cloud(point_cloud_output_path, masked_pc)
    print("...... Point cloud stored as '{}'.".format(point_cloud_output_path))

    if plot_path is not None:
        os.makedirs(plot_path, exist_ok=True)
        # store masked segmentation output
        colorized_mask_path = os.path.join(plot_path, "{}_segmask.png".format(frame))
        store_colorized_mask(seg_image, rgb_img, colorized_mask_path, class_id)
        # capture image of 3D visualization and store
        _3d_vis_path = os.path.join(plot_path, "{}_cloud.png".format(frame))
        store_3d_vis(masked_pc, rgb_img, _3d_vis_path)
        print("...... Plots stored as '{}' and '{}'.".format(colorized_mask_path, _3d_vis_path))


@timeit
def main(image_path, depth_path, meta_path, output_path, model_path, class_id, frame, plot_path, use_gpu):
    # check if cuda available
    dev = "cuda" if torch.cuda.is_available() and use_gpu else "cpu"
    device = torch.device(dev)
    # check if model path exists
    if not os.path.isfile(model_path):
        raise FileNotFoundError("ERROR: The specified model_path {} does not exist. -- Aborted point cloud creation."
                                .format(model_path))
    model_path = os.path.abspath(model_path)

    # instantiate model
    model = segnet()
    model.load_state_dict(torch.load(model_path, map_location=dev))
    model = model.to(device)

    # run inference
    model.eval()
    _predict_one_pc(model, device, image_path, depth_path, meta_path,
                    output_path, class_id, frame, plot_path)


if __name__ == '__main__':
    args = _parse_args()
    main(args.image_path,
         args.depth_path,
         args.meta_path,
         args.output_path,
         args.model_path,
         args.class_id,
         args.frame,
         args.plot_path,
         args.use_gpu)
