import os

import numpy as np
import open3d as o3d
import scipy.io as scio
from PIL import Image
import torch
from torch.autograd import Variable
from torchvision import transforms

from lib.utils import timeit
from vanilla_segmentation.pc_util import create_point_cloud, canonical_transform
from vanilla_segmentation.segnet import SegNet as segnet
from vanilla_segmentation.plot_util import store_3d_vis, store_colorized_mask

MODEL_PATH = 'MODEL_PATH'
USE_GPU = 'USE_GPU'


class Segmentor:

    def __init__(self):
        self._model_path = os.getenv(MODEL_PATH)
        self._use_gpu = os.getenv(USE_GPU, False)
        self.model = self._load_model()
        self.model.eval()

    @property
    def _device(self):
        dev = "cuda" if torch.cuda.is_available() and self._use_gpu else "cpu"
        return torch.device(dev)

    def _load_model(self):
        if not os.path.isfile(self._model_path):
            raise FileNotFoundError(
                "ERROR: The specified model_path {} does not exist. -- Aborted point cloud creation."
                    .format(self._model_path))
        model_path = os.path.abspath(self._model_path)

        # instantiate model
        model = segnet()
        model.load_state_dict(torch.load(model_path, map_location=self._device))
        return model.to(self._device)

    def _get_and_preprocess_rgb(self, image_path):
        # load rgb image from path
        rgb_img = Image.open(image_path).convert("RGB")
        rgb = np.array(rgb_img)
        # prepare for inference in SegNet
        rgb = np.transpose(rgb, (2, 0, 1))
        norm = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        rgb = norm(torch.from_numpy(rgb.astype(np.float32)))
        rgb = rgb.unsqueeze(0)
        return rgb, rgb_img

    def _postprocess_output(self, seg_data):
        # adjust output from SegNet
        seg_data2 = seg_data[0]
        seg_data2 = torch.transpose(seg_data2, 0, 2)
        seg_data2 = torch.transpose(seg_data2, 0, 1)
        seg_image = torch.argmax(seg_data2, dim=-1)
        seg_image = seg_image.detach().cpu().numpy()
        return seg_image

    def _get_relevant_data(self, image_path, depth_path, meta_path):
        # load meta, color, and depth files for point cloud generation
        meta = scio.loadmat(meta_path)
        color_img = np.array(Image.open(image_path))
        depth = np.array(Image.open(depth_path))
        return meta, color_img, depth

    def _create_pc(self, class_id, label, color, depth, meta):
        # apply predicted mask to depth map and create point cloud for only the class
        masked_pc = create_point_cloud(color, depth, meta, label, class_id)
        # rotate output to canonical transform
        masked_pc = canonical_transform(masked_pc, meta, class_id)
        return masked_pc

    def store_partial_point_cloud(self, frame, masked_pc, output_path):
        # store segmented model
        point_cloud_output_path = os.path.join(output_path, '{}_pred_cloud.pcd'.format(frame))
        o3d.io.write_point_cloud(point_cloud_output_path, masked_pc)
        print("...... Point cloud stored as '{}'.".format(point_cloud_output_path))

    def store_output_images(self, class_id, frame, masked_pc, plot_path, rgb_img, seg_image):
        os.makedirs(plot_path, exist_ok=True)
        # store masked segmentation output
        colorized_mask_path = os.path.join(plot_path, "{}_segmask.png".format(frame))
        store_colorized_mask(seg_image, rgb_img, colorized_mask_path, class_id)
        # capture image of 3D visualization and store
        _3d_vis_path = os.path.join(plot_path, "{}_cloud.png".format(frame))
        store_3d_vis(masked_pc, rgb_img, _3d_vis_path)
        print("...... Plots stored as '{}' and '{}'.".format(colorized_mask_path, _3d_vis_path))

    @timeit
    def create_partial_point_cloud(self, image_path, depth_path,
                                   meta_path, output_path,
                                   class_id, frame,
                                   plot_path=None):
        print("...... Segmenting frame '{}'...".format(frame))

        # load and preprocess image
        rgb, rgb_img = self._get_and_preprocess_rgb(image_path)

        # run inference and postprocess output
        rgb = Variable(rgb).to(self._device)
        seg_data = self.model(rgb)
        seg_image = self._postprocess_output(seg_data)

        # check if wanted class is in the image
        if class_id not in seg_image:
            raise FileNotFoundError(" -- The class id {} is not in frame {}. -- Aborted point cloud creation."
                                    .format(class_id, frame))

        # get data needed for pc creation
        meta, color_img, depth = self._get_relevant_data(image_path, depth_path, meta_path)

        # create point cloud in canonical transform for segment only
        print("...... Creating point cloud...")
        masked_pc = self._create_pc(class_id, seg_image, color_img, depth, meta)

        self.store_partial_point_cloud(frame, masked_pc, output_path)

        if plot_path is not None:
            self.store_output_images(class_id, frame, masked_pc, plot_path, rgb_img, seg_image)
