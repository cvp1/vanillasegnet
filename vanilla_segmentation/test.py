import os
import argparse
import time
from uuid import uuid4

import torch
from torch.autograd import Variable

from lib.utils import setup_logger, AverageValueMeter
from vanilla_segmentation.data_controller import SegDataset
from vanilla_segmentation.loss import Loss
from vanilla_segmentation.segnet import SegNet


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset_root', default='/home/data1/jeremy/YCB_Video_Dataset',
                        help="dataset root dir (''YCB_Video Dataset'')")
    parser.add_argument('--batch_size', default=1, help="batch size")
    parser.add_argument('--workers', type=int, default=0, help='number of data loading workers')
    parser.add_argument('--use_gpu', default="True", help="Whether to use GPU or not")
    parser.add_argument('--logs_path', default='logs/', help="path to save logs")
    parser.add_argument('--model_path', default='trained_models/', help="Path to model weights to use.")
    return parser.parse_args()


def load_checkpoint(model_path, logger):
    logger.info("Loading checkpoint '{}'".format(model_path))
    checkpoint = torch.load(model_path, map_location=lambda storage, loc: storage)
    return checkpoint


def remove_existing_logs(log_dir):
    print("Removing existing logs in {}".format(log_dir))
    for log in os.listdir(log_dir):
        os.remove(os.path.join(log_dir, log))


def create_log_dir_if_not_exists(log_dir):
    os.makedirs(log_dir, exist_ok=True)


def main(opt):
    run_id = uuid4()
    start_time = time.time()

    create_log_dir_if_not_exists(opt.logs_path)
    remove_existing_logs(opt.logs_path)

    logger = setup_logger('test', os.path.join(opt.logs_path, 'log_run_id_{}.txt'.format(run_id)))
    logger.info("Starting test run {}".format(run_id))

    data_list_txt = os.path.abspath(os.path.join(__file__,
                                                 os.pardir,
                                                 '../datasets/ycb/dataset_config/holdout_data_list.txt'))
    line_count = len(open(data_list_txt).readlines())
    test_dataset = SegDataset(opt.dataset_root, data_list_txt, False, line_count)
    test_dataloader = torch.utils.data.DataLoader(test_dataset,
                                                  batch_size=opt.batch_size,
                                                  shuffle=False,
                                                  num_workers=int(opt.workers))

    test_dataset_size = len(test_dataset)
    logger.info("Test dataset size: {}".format(test_dataset_size))

    use_gpu = True if opt.use_gpu is not "" and torch.cuda.is_available() else False
    device = torch.device("cuda" if use_gpu else "cpu")
    logger.info("Using device {}".format(device))

    model = SegNet()
    model = model.to(device)
    model.load_state_dict(load_checkpoint(opt.model_path, logger))
    model.eval()

    test_loss = AverageValueMeter()
    criterion = Loss()

    logger.info('Test time {0}'.format(
        time.strftime("%Hh %Mm %Ss", time.gmtime(time.time() - start_time)) + ', ' + 'Testing started'))

    for batch_number, data in enumerate(test_dataloader, 1):
        with torch.no_grad():
            rgb, target, path_index = data
            print(path_index)
            rgb, target = rgb.to(device), target.to(device)
            semantic = model(rgb)
            semantic_loss = criterion(semantic, target)
            batch_loss = semantic_loss.item()
            test_loss.update(batch_loss)
            logger.info('{0} -- Batch: {1}/{2} CEloss: {3}'.format(
                time.strftime("%Hh %Mm %Ss", time.gmtime(time.time() - start_time)),
                batch_number,
                int(test_dataset_size / opt.batch_size),
                batch_loss))
        # torch.cuda.empty_cache()

    logger.info("Done. Mean CEloss: {0}, Std: {1}".format(test_loss.mean, test_loss.std))


if __name__ == '__main__':
    opt = parse_args()
    main(opt)
